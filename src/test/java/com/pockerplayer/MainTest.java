package com.pockerplayer;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    private static WebDriver driver;
    private static TestData data = new TestData();
    private static TestElements locators = new TestElements();
    private static WebDriverWait wait;

    public static void main(String[] args){
        InsertingPlayerTest();
        EditingPlayerTest();
        DeletingPlayerTest();
    }

    @BeforeSuite
    private static void SetupWebDriver() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver,data.waitTime);
    }

    @BeforeTest
    private static void LoginToAdminArea(){
        driver.get(data.serverAddress);
        driver.findElement(By.id(locators.fieldLoginNameId)).sendKeys(data.adminLogin);
        driver.findElement(By.id(locators.fieldLoginPasswordId)).sendKeys(data.adminPassword);
        driver.findElement(By.id(locators.btnLoginId)).click();
    }

    @Test
    public static void InsertingPlayerTest() {
        //Test Data
        ArrayList<PockerPlayer> players = data.getPlayers();
        //Steps
        InsertPlayer(players.get(0));
        SavePlayerRecord();
        FindPlayer(players.get(0));
        OpenPlayersRecordToEdit(players.get(0));
        VerifyPlayersFields(players.get(0));
        SavePlayerRecord();
        FindPlayer(players.get(0));
        DeletePlayer(players.get(0));
    }

    @Test
    public static void EditingPlayerTest(){
        //Test Data
        ArrayList<PockerPlayer> players = data.getPlayers();
        //Steps
        InsertPlayer(players.get(0));
        SavePlayerRecord();
        FindPlayer(players.get(0));
        OpenPlayersRecordToEdit(players.get(0));
        EditPlayer(players.get(1));
        SavePlayerRecord();
        FindPlayer(players.get(0));
        OpenPlayersRecordToEdit(players.get(0));
        VerifyPlayersFields(players.get(1));
        SavePlayerRecord();
        FindPlayer(players.get(0));
        DeletePlayer(players.get(0));
    }

    @Test
    public static void DeletingPlayerTest(){
        //Test Data
        ArrayList<PockerPlayer> players = data.getPlayers();
        //Steps
        InsertPlayer(players.get(0));
        SavePlayerRecord();
        FindPlayer(players.get(0));
        VerifyPlayerIsInList(players.get(0));
        DeletePlayer(players.get(0));
        FindPlayer(players.get(0));
        VerifyPlayerIsNotInList(players.get(0));
    }

    private static void InsertPlayer(PockerPlayer player){
        wait.until(ExpectedConditions.titleIs(locators.titleAdminArea));
        driver.findElement(By.xpath(locators.btnInsertPlayerXPath)).click();
        wait.until(ExpectedConditions.elementToBeClickable(
                By.id(locators.fieldInsertUsernameId)));
        driver.findElement(By.id(locators.fieldInsertUsernameId)).sendKeys(player.getUserName());
        driver.findElement(By.id(locators.fieldInsertEmailId)).sendKeys(player.getEmail());
        driver.findElement(By.id(locators.fieldInsertPasswordId)).sendKeys(player.getPassword());
        driver.findElement(By.id(locators.fieldInsertConfirmPasswordId)).sendKeys(player.getPassword());
        driver.findElement(By.id(locators.fieldInsertFirstName)).sendKeys(player.getFirstName());
        driver.findElement(By.id(locators.fieldInsertLastName)).sendKeys(player.getLastName());
        driver.findElement(By.id(locators.fieldInsertCity)).sendKeys(player.getCity());
        driver.findElement(By.id(locators.fieldInsertAddress)).sendKeys(player.getAddress());
        driver.findElement(By.id(locators.fieldInsertPhone)).sendKeys(player.getPhone());
    }

    private static void FindPlayer(PockerPlayer player){
        wait.until(ExpectedConditions.titleIs(locators.titleAdminArea));
        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldAdminPlayerNameId)));
        driver.findElement(By.xpath(locators.btnAdminResetXPath)).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldAdminPlayerNameId)));
        WebElement searchField = driver.findElement(By.id(locators.fieldAdminPlayerNameId));
        searchField.clear();
        searchField.sendKeys(player.getUserName());
        driver.findElement(By.xpath(locators.btnAdminSearchXPath)).click();
        wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath(locators.selectAdminBottomXPath)));
        wait.until(ExpectedConditions.invisibilityOf(
                driver.findElement(By.id(locators.processingGridId))));
    }

    private static void OpenPlayersRecordToEdit (PockerPlayer player){
        String btnAdminEditXPath = locators.getbtnAdminEditXPath(player);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(locators.tableRowsSelection),0));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(btnAdminEditXPath)));
        driver.findElement(By.xpath(btnAdminEditXPath)).click();
    }

    private static void EditPlayer (PockerPlayer player){
        wait.until(ExpectedConditions.titleIs(locators.titleEditPlayerPage));
        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldEditEmailId)));
        WebElement email = driver.findElement(By.id(locators.fieldEditEmailId));
        WebElement f_name = driver.findElement(By.id(locators.fieldEditFirsNameId));
        WebElement l_name = driver.findElement(By.id(locators.fieldEditLastNameId));
        WebElement city = driver.findElement(By.id(locators.fieldEditCityId));
        WebElement address = driver.findElement(By.id(locators.fieldEditAddressId));
        WebElement phone = driver.findElement(By.id(locators.fieldEditPhoneId));

        email.clear();
        email.sendKeys(player.getEmail());
        f_name.clear();
        f_name.sendKeys(player.getFirstName());
        l_name.clear();
        l_name.sendKeys(player.getLastName());
        city.clear();
        city.sendKeys(player.getCity());
        address.clear();
        address.sendKeys(player.getAddress());
        phone.clear();
        phone.sendKeys(player.getPhone());
    }

    private static void SavePlayerRecord (){
        wait.until(ExpectedConditions.or(ExpectedConditions.titleIs(locators.titleInsertPlayerPage),
                ExpectedConditions.titleIs(locators.titleEditPlayerPage)));

        //click on needed button for Save new User / Save changes into existing User
        if(driver.getTitle().equals(locators.titleInsertPlayerPage)){
            driver.findElement(By.id(locators.btnSubmitNewUserId)).click();
        }else if(driver.getTitle().equals(locators.titleEditPlayerPage)){
            driver.findElement(By.id(locators.btnEditSubmitId)).click();
        }

        //Confirm changes for Edit user page
        if (driver.getTitle().equals(locators.titleEditPlayerPage)){
            wait.until(ExpectedConditions.alertIsPresent());
            driver.switchTo().alert().accept();
        }
    }

    private static void VerifyPlayersFields(PockerPlayer player){
        String userEmail, userFirstName, userLastName, userCity, userAddress, userPhone;
        wait.until(ExpectedConditions.elementToBeClickable(By.id(locators.fieldEditEmailId)));
        userEmail = driver.findElement(By.id(locators.fieldEditEmailId)).getAttribute("value");
        userFirstName = driver.findElement(By.id(locators.fieldEditFirsNameId)).getAttribute("value");
        userLastName = driver.findElement(By.id(locators.fieldEditLastNameId)).getAttribute("value");
        userCity = driver.findElement(By.id(locators.fieldEditCityId)).getAttribute("value");
        userAddress = driver.findElement(By.id(locators.fieldEditAddressId)).getAttribute("value");
        userPhone = driver.findElement(By.id(locators.fieldEditPhoneId)).getAttribute("value");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(userEmail,player.getEmail());
        softAssert.assertEquals(userFirstName,player.getFirstName());
        softAssert.assertEquals(userLastName,player.getLastName());
        softAssert.assertEquals(userCity,player.getCity());
        softAssert.assertEquals(userAddress,player.getAddress());
        softAssert.assertEquals(userPhone,player.getPhone());
        softAssert.assertAll();
    }

    private static void VerifyPlayerIsInList(PockerPlayer player) {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locators.getPlayerLink(player))));
        Assert.assertEquals(driver.findElement(By.xpath(locators.getPlayerLink(player))).getAttribute("text"),
                player.getUserName());
    }

    private static void VerifyPlayerIsNotInList(PockerPlayer player) {
        wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath(locators.selectAdminBottomXPath)));
        wait.until(ExpectedConditions.invisibilityOf(driver.findElement(
                By.id(locators.processingGridId))));

        List <WebElement> listOfPlayers = driver.findElements(By.xpath(locators.usersListLocator));
        Assert.assertTrue(listOfPlayers.isEmpty());
    }

    private static void DeletePlayer(PockerPlayer player){
        wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath(locators.selectAdminBottomXPath)));

        wait.until(ExpectedConditions.elementToBeClickable(
                driver.findElement(By.xpath(locators.getbtnAdminDeleteXPath(player)))));
        driver.findElement(By.xpath(locators.getbtnAdminDeleteXPath(player))).click();
        driver.switchTo().alert().accept();
    }

    @AfterTest
    private static void Logout(){
        driver.findElement(By.xpath(locators.btnLogoutXPath)).click();
    }

    @AfterSuite
    private static void BrowserQuit(){
        driver.quit();
    }


}
