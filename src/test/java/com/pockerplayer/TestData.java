package com.pockerplayer;

import java.util.ArrayList;

class TestData {

    String serverAddress = "http://80.92.229.235/auth/login";
    String adminLogin = "admin";
    String adminPassword = "test";
    int waitTime = 50;
    String messageSuccess = "Player have been added successfully";

    ArrayList<PockerPlayer> getPlayers(){
        int number = (int) (Math.random() * 1000000000);
        String username = "Re" + number;
        ArrayList <PockerPlayer> testData = new ArrayList<>(2);
        testData.add(new PockerPlayer(username, "QwE" + number, username + "@hosting.com",
                username + "_firstName", username + "_lastName", username + "_HomeCity",
                username + " Home Address in the City of Users",username + "Phone"));
        testData.add(new PockerPlayer(username, "AsD" + number, username + "@gmail.com",
                username + "_SimpleName",username + "_FamilyName", username + "_BestCity",
                username + " Home of User",username + "Cellphone"));
        return testData;
    }
}
