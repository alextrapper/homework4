package com.pockerplayer;

class TestElements {

    // Fields
    final String fieldLoginNameId = "username";
    final String fieldLoginPasswordId = "password";

    final String fieldAdminPlayerNameId = "login";

    final String fieldInsertUsernameId = "us_login";
    final String fieldInsertEmailId = "us_email";
    final String fieldInsertConfirmPasswordId = "confirm_password";
    final String fieldInsertPasswordId = "us_password";
    final String fieldInsertFirstName = "us_fname";
    final String fieldInsertLastName = "us_lname";
    final String fieldInsertCity = "us_city";
    final String fieldInsertAddress = "us_address";
    final String fieldInsertPhone = "us_phone";

    final String fieldEditEmailId = "us_email";
    final String fieldEditFirsNameId = "us_fname";
    final String fieldEditLastNameId = "us_lname";
    final String fieldEditCityId = "us_city";
    final String fieldEditAddressId = "us_address";
    final String fieldEditPhoneId = "us_phone";

    //alerts and messages
    final String fieldMessages = "messages";
    final String processingGridId = "grid_processing";



    //Buttons
    final String btnLoginId = "logIn";

    final String btnAdminSearchXPath = "//button[@type='submit']";
    final String btnInsertPlayerXPath = "//a[@href='/players/insert']";
    final String btnSubmitNewUserId = "Submit";
    final String btnAdminResetXPath = "//button[@type='reset']";
    String getbtnAdminEditXPath (PockerPlayer player){
        return "//a[text()='" + player.getUserName() + "']//..//..//a[@title='Edit']";
    }
    String getbtnAdminDeleteXPath (PockerPlayer player) {
        return "(//a[text()='" + player.getUserName() +  "']//..//..)//a[@title='Delete']";
    }
    String getPlayerLink (PockerPlayer player){
        return "//a[text()='" + player.getUserName() + "']";
    }
    final String btnEditCancelId = "Cancel";
    final String btnEditSubmitId = "Submit";
    final String btnLogoutXPath = "//a[contains(@href, 'logout')]";
    final String btnDeletePlayerXPath = "//table[@id=\"grid\"]//tr[1]//a[contains(@class, \"delete\")]";




    // Titles
    final String titleAdminArea = "Players";
    final String titleInsertPlayerPage = "Players - Insert";
    final String titleEditPlayerPage = "Players - Edit";

    /*
    for Conditions
    */
    final String tableRowsSelection = "//table[@id='grid']//tbody//tr";
    final int expectedElementsInSelection = 1;
    final String tableColumnPlayersName = "//table[@id='grid']//tbody//tr[1]//td[3]//a";
    final String usersListLocator = "//a[contains(@href, '/players/edit/popup')]";
    final String selectAdminBottomXPath = "//div[@class='bottom']//select[@name='grid_length']";





}
